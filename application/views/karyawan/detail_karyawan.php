<style>
    td {
        padding: 10px;
    }
    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
<div class="container-fluid">
	<div class="block-header">
		<ol class="breadcrumb breadcrumb-col-pink">
			<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
			<li class="active"><i class="material-icons">library_books</i> <?php echo $title; ?></li>
		</ol>
	</div>
	<!-- Basic Card -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						Detail Karyawan
					</h2>
				</div>
				<div class="body">
                    <div class="row" style="padding-top: 10px;">
                        <?php foreach($karyawan as $kar){ ?>
                        <div class="col-md-4">
                            <center>
                            <i class="material-icons" style="font-size: 175px; padding-top: 10px;">
                                account_circle
                            </i>
                            <h3 style="margin-top: 0px"><?php echo $kar['finish_task'] ?>/<?php echo $kar['jml_task'] ?></h3>
                            </center>
                        </div>
                        <div class="col-md-7" style="border: 1px solid black; margin-left: 2%; padding: 20px 20px 20px; border-radius: 10px;">
                            <table border="0px" width="100%">
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td><?php echo $kar['nama'] ?></td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>:</td>
                                    <td><?php echo $kar['alamat'] ?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td><?php echo $kar['email'] ?></td>
                                </tr>
                                <tr>
                                    <td>Nomor Telpon</td>
                                    <td>:</td>
                                    <td><?php echo $kar['no_telp'] ?></td>
                                </tr>
                            </table>
                        </div>
                        <?php } ?>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary waves-effect pull-right" data-toggle="modal" data-target="#add">
                                <i class="material-icons">person_add</i>
                                <span>Tambah Task</span>
                            </button><br><br>
                            <div class="tab" style="margin-top: 10px;">
                                <button type="button" class="tablinks <?php if ($this->uri->segment(4) != '') {
                                                                            if ($this->uri->segment(4) == 'waiting') {
                                                                                echo 'active';
                                                                            }
                                                                        } else {
                                                                            echo 'active';
                                                                        } ?>" onclick="openCity(event, 'Waiting')">Waiting</button>
                                <button type="button" class="tablinks <?php if ($this->uri->segment(4) != '') {
                                                                            if ($this->uri->segment(4) == 'onprog') {
                                                                                echo 'active';
                                                                            }
                                                                        } ?>" onclick="openCity(event, 'Onprog')">On Progress</button>
                                <button type="button" class="tablinks <?php if ($this->uri->segment(4) != '') {
                                                                            if ($this->uri->segment(4) == 'finish') {
                                                                                echo 'active';
                                                                            }
                                                                        } ?>" onclick="openCity(event, 'Finish')">Finish</button>
                            </div>
                            <div id="Waiting" class="tabcontent" style="<?php if ($this->uri->segment(4) != '') {
                                                                    if ($this->uri->segment(4) == 'waiting') {
                                                                        echo 'display: block;';
                                                                    }
                                                                } else {
                                                                    echo 'display: block;';
                                                                } ?>">
                                <div class="row" style="padding-top: 10px;">
                                <?php foreach($task_waiting as $r){ ?>
                                <div class="col-md-11" style="border: 1px solid black; margin-left: 4%; border-radius: 10px; padding: 10px 10px 10px;">
                                    <div class="col-md-1">
                                    <i class="material-icons" style="font-size: 48px;">
                                        perm_contact_calendar
                                    </i>
                                    </div>
                                    <div class="col-md-9">
                                        <?php echo $r['nama_task'] ?><br>
                                        <?php echo $r['deadline'] ?><br>
                                        <a href="<?php echo base_url() ?>Task/detailTaskKaryawan/<?php echo $r['id_task'] ?>">
                                            <i class="material-icons">
                                                remove_red_eye
                                            </i>
                                        </a>
                                        <a href="<?php echo base_url() ?>Task/deleteTask/<?php echo $r['id_task'] ?>/kar/<?php echo $this->uri->segment(3); ?>">
                                            <i class="material-icons">
                                                delete
                                            </i>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $r['created_at'] ?><br>
                                        <span class="label label-info">Waiting</span>
                                    </div>
                                </div>
                                <?php } ?>
                                </div>
                            </div>
                            <div id="Onprog" class="tabcontent" style="<?php if ($this->uri->segment(4) != '') {
                                                                    if ($this->uri->segment(4) == 'onprog') {
                                                                        echo 'display: block;';
                                                                    }
                                                                } ?>">
                                <div class="row" style="padding-top: 10px;">
                                <?php foreach($task_onprog as $h){ ?>
                                <div class="col-md-11" style="border: 1px solid black; margin-left: 4%; border-radius: 10px; padding: 10px 10px 10px;">
                                    <div class="col-md-1">
                                    <i class="material-icons" style="font-size: 48px;">
                                        perm_contact_calendar
                                    </i>
                                    </div>
                                    <div class="col-md-9">
                                        <?php echo $h['nama_task'] ?><br>
                                        <?php echo $h['deadline'] ?><br>
                                        <a href="<?php echo base_url() ?>Task/detailTaskKaryawan/<?php echo $h['id_task'] ?>">
                                            <i class="material-icons">
                                                remove_red_eye
                                            </i>
                                        </a>
                                        <a href="<?php echo base_url() ?>Task/deleteTask/<?php echo $r['id_task'] ?>/kar/<?php echo $this->uri->segment(3); ?>">
                                            <i class="material-icons">
                                                delete
                                            </i>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $h['created_at'] ?><br>
                                        <span class="label label-warning">On Progress</span>
                                    </div>
                                </div>
                                <?php } ?>
                                </div>
                            </div>
                            <div id="Finish" class="tabcontent" style="<?php if ($this->uri->segment(4) != '') {
                                                                    if ($this->uri->segment(4) == 'finish') {
                                                                        echo 'display: block;';
                                                                    }
                                                                } ?>">
                                <div class="row" style="padding-top: 10px;">
                                <?php foreach($task_finish as $h){ ?>
                                <div class="col-md-11" style="border: 1px solid black; margin-left: 4%; border-radius: 10px; padding: 10px 10px 10px;">
                                    <div class="col-md-1">
                                    <i class="material-icons" style="font-size: 48px;">
                                        perm_contact_calendar
                                    </i>
                                    </div>
                                    <div class="col-md-9">
                                        <?php echo $h['nama_task'] ?><br>
                                        <?php echo $h['deadline'] ?><br>
                                        <a href="<?php echo base_url() ?>Task/detailTaskKaryawan/<?php echo $h['id_task'] ?>">
                                            <i class="material-icons">
                                                remove_red_eye
                                            </i>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $h['created_at'] ?><br>
                                        <span class="label label-success">Finish</span>
                                    </div>
                                </div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="add" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="largeModalLabel">Tambah Task</h4>
			</div>
			<form action="<?php echo base_url() ?>Task/addTask/<?php echo $this->uri->segment(3); ?>" method="POST">
				<div class="modal-body">
                    <label for="nama_task">Nama Task</label>
					<div class="form-group">
						<div class="form-line">
							<input type="text" name="nama_task" class="form-control"
								placeholder="Masukkan nama reward">
						</div>
                    </div>
					<label for="deskripsi">Deskripsi</label>
					<div class="form-group">
						<div class="form-line">
							<textarea name="deskripsi" name="deskripsi" rows="4" class="form-control no-resize"
								placeholder="Masukkan deskripsi reward"></textarea>
						</div>
					</div>
					<label for="deadline">Deadline</label>
					<div class="form-group">
                        <div class="form-line" id="bs_datepicker_container">
                            <input type="date" name="deadline" class="form-control" placeholder="Please choose a date...">
                        </div>
                    </div>
                    <label for="poin">Poin</label>
					<div class="form-group">
						<div class="form-line">
							<input type="text" name="poin" class="form-control"
								placeholder="Masukkan nama reward">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-link waves-effect">SIMPAN</button>
					<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">BATAL</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    function openCity(evt, command) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(command).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>