<div class="container-fluid">
	<div class="block-header">
		<ol class="breadcrumb breadcrumb-col-pink">
			<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
			<li class="active"><i class="material-icons">library_books</i> <?php echo $title; ?></li>
		</ol>
	</div>
	<!-- Basic Card -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						Detail Task
					</h2>
				</div>
				<div class="body">
                    <div class="row" style="padding-top: 10px;">
                        <?php foreach($task as $k){ ?>
                            <form action="<?php echo base_url() ?>Task/editTask/<?php echo $k['id_task'] ?>/<?php echo $this->uri->segment(3); ?>" method="POST" style="padding-left: 20px; padding-right: 20px;">
                            <label for="nama_task">Nama Task</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="nama_task" class="form-control"
                                        placeholder="Masukkan nama reward" value="<?php echo $k['nama_task'] ?>">
                                </div>
                            </div>
                            <label for="deskripsi">Deskripsi</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea name="deskripsi" name="deskripsi" rows="4" class="form-control no-resize"
                                        placeholder="Masukkan deskripsi reward"><?php echo $k['deskripsi'] ?></textarea>
                                </div>
                            </div>
                            <label for="deadline">Deadline</label>
                            <div class="form-group">
                                <div class="form-line" id="bs_datepicker_container">
                                    <input type="date" name="deadline" value="<?php echo $k['deadline'] ?>" class="form-control" placeholder="Please choose a date...">
                                </div>
                            </div>
                            <label for="poin">Poin</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="poin" class="form-control"
                                        placeholder="Masukkan nama reward" value="<?php echo $k['poin'] ?>">
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn btn-warning pull-right">UPDATE</button>
                            </form>
                        <?php } ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>